 (function(){ 
	 $(document).ready(function(){
		$("#accordion-resizer").hide(); 
		 initAccordian();
		 bindEvents();
		
	 });
	 
	 var bindEvents=function(){
		 $("#searchBox").bind("keyup",function(){
			 $("#accordion-resizer").hide(); 
			 if($("#searchBox").val().trim()!="")
			 {
				 searchService.getPath($("#searchBox").val()).then(function(resultObj){
					 constructAccordian(resultObj);
					 $( "#accordion" ).accordion( "refresh" );
				 });
			 }
		 })
		 
	 }
	 
	var initAccordian=function(){
		   $( "#accordion" ).accordion({
		      heightStyle: "fill"
		   });
		 
		   $( "#accordion-resizer" ).resizable({
		      minHeight: 240,
		      minWidth: 200,
		      resize: function() {
		        $( "#accordion" ).accordion( "refresh" );
		      }
		   });
		   
	}
 
	var constructAccordian=function(resultObj){
		console.log(JSON.stringify(resultObj));
		
		var str="";
		for(var attr in resultObj)//looping list of search key
		{
			$("#accordion-resizer").show(); 
			str+="<h3>"+attr+" - Founds in "+resultObj[attr].length+" Path</h3>"
			var resultList= JSON.parse(JSON.stringify(resultObj[attr]));
			str+="<div><ul>";
			for(var result in resultList)
			{
				
			    str+="<li>"+resultList[result]+"</li>";
			    
			}
			str+="</ul></div>";
		}
		$("#accordion").html(str);
	} 
 
})();