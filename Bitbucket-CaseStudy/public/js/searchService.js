var searchService=(function(){
	var lookup = new TreeLookup();
	
	///Global/shared variables
	var data={
			root:"/",
			searchKey:"",
			result:{}
	}
	var getPath=function(searchData){
		data.result={};
		data.searchKey=searchData.split(",");
		return search(data.root);
		
		
	}
	var childrenDataHandler=function (nodesFromPromise,path,resolve) {
		var arrayPromise=[];
		if(nodesFromPromise.length==0)
			 resolve(data.result);
       else{
    	   //LOOPING EACH LIST AND CHECK DATA WITH SEARCH KEY	
	       	for(var item in nodesFromPromise)
	       	{
	       		processItemCheck(path,nodesFromPromise[item],data.searchKey);
	       		arrayPromise.push(search(path+nodesFromPromise[item]+"/"));
	       	}//for
       }//list check
	  
		Promise.all(arrayPromise).then(function(){
			resolve(data.result);
		});
		
   }
	var search=function(path)
	{
		
		var lookupPromise=lookup.getChildrenAsPromise(path);
		var p=new Promise(function(resolve,reject){
			lookupPromise.then(function(nodesFromPromise){
				childrenDataHandler(nodesFromPromise,path,resolve);
			});
			
		});
		
		return p;

	}//searchFunction
	
	var processItemCheck=function(path,nodeValue,searchKeyList)
	{
		for(var key in searchKeyList)
		{
			
			var searchValue=searchKeyList[key];
			if($.trim(searchValue)=="")
				continue;
			if(data.result[searchValue]==undefined){ data.result[searchValue]=[];}
			if(nodeValue==searchValue)
    		{
    			data.result[searchValue].push(path);
    			console.log("Found:"+path)
    		}
		}
		
	}
	return{
		getPath:getPath
	}
	
})();